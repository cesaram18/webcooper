from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.shortcuts import render


class HomePageView(TemplateView):
    template_name = "core/base.html"
 
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'title': 'Cooper'})

class SamplePageView(TemplateView):
     template_name = "core/sample.html"
